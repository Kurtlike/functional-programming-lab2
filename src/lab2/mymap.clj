(ns lab2.mymap
  (:import (clojure.lang IPersistentMap Associative Util ILookup IMapEntry Seqable IPersistentCollection IMeta MapEquivalence IPersistentVector)
           (java.util Map)))
(declare ->MyMapEntry)
(defrecord MyMapEntry [key val]
  IMapEntry
  (getKey [_] key)
  (getValue [_] val))

(defn get-not-empty
  [arr]
  (filter
   #(not (nil? %)) arr))

(defn find-entry [arr key]
  (if (not= (count arr) 0)
    (loop [computed (-> key hash int Math/abs)
           left (count arr)]
      (let [pos (rem computed (count arr))]
        (if-let [entry (nth arr pos)]
          (if (.equals key (:key entry))
            [entry pos]
            (if (> left 0)
              (recur (inc computed) (dec left)) [nil nil]))
          [nil pos]))) [nil nil]))

(defn insert-entries
  [amount arr new-entries]
  (if (or (empty? new-entries) (<= amount 0))
    [arr new-entries]
    (if-let [pos (last (find-entry arr (:key (first new-entries))))]
      (recur (dec amount) (assoc arr pos (first new-entries)) (rest new-entries))
      (println "Reached nil: " amount "arr: " arr "new: " new-entries "pos:" (last (find-entry arr (:key (first new-entries))))))))

(defn balance
  ([arr] (balance arr 2))
  ([arr coef]
   (let [current-size (count arr)
         filtered (get-not-empty arr)]
     (->> filtered
          (insert-entries (count filtered) (vec (repeat (* coef (max current-size 1)) nil)))
          first))))

(defn insert [load arr new]
  (let [next-load (+ load (/ (double 1) (count arr)))]
    (if (>= next-load 0.8)
      (let [balance (balance arr)
            balanced-load (/ (-> balance get-not-empty count double) (count balance))]
        (insert balanced-load balance new))
      [(first (insert-entries 1 arr [new])) next-load])))

(defn delete [arr key]
  (if-let [pos (last (find-entry arr key))]
    (filter #(not (.equals key (:key %))) (get-not-empty arr))
    arr))
(declare ->MyHashMap)
(deftype MyHashMap [contents load]

  ILookup
  (valAt [m k]
    (if-let [[attempt _] (find-entry contents k)]
      (:val attempt)
      nil))

  Iterable
  (iterator [m] (.iterator (seq m)))

  Seqable
  (seq [_] (get-not-empty contents))

  IPersistentMap
  (assoc [_ k v] (apply ->MyHashMap (insert load contents (->MyMapEntry k v))))
  (assocEx [m k v] (if (.containsKey m k)
                     (.runtimeException Util "Key already present")
                     (.assoc m k v)))
  (without [_ k] (->MyHashMap (delete contents k) load))

  IPersistentCollection
  (count [m] (count (get-not-empty contents)))
  (cons [m new] (cond
                  (and (instance? IPersistentVector new) (>= (count new) 2)) (assoc m (first new) (nth new 2))
                  (instance? IMapEntry new) (assoc m (key new) (val new))
                  (instance? Seqable new) (reduce #(assoc %1 (key %2) (val %2)) m (seq new))))
  (empty [_] (->MyHashMap [] 1.0))
  (equiv [m o]
    (if (or
         (not (or (instance? Map o) (instance? IPersistentMap o)))
         (and
          (instance? IPersistentMap o)
          (->> o (instance? MapEquivalence) not))
         (not= (count o) (count m)))
      false
      (loop [elems (seq m)]
        (let [cur-elem (first elems)]
          (if-not (empty? elems)
            (if (or
                 (not (contains? o (.getKey cur-elem)))
                 (not (= (.getValue cur-elem) (get o (.getKey cur-elem)))))
              false
              (recur (rest elems)))
            true)))))
  Associative
  (containsKey [_ k] (let [[attempt _] (find-entry contents k)]
                       (if-not (nil? attempt)
                         true
                         false)))
  (entryAt [_ k] (first (find-entry contents k))))

(defn oa-map
  ([]  (->MyHashMap [nil nil nil nil] 0.0))
  ([src-map] (oa-map src-map 2))
  ([src-map coef] (->MyHashMap (->>
                                src-map
                                (map #(->MyMapEntry (first %) (last %)))
                                (#(balance % coef))) (/ 1.0 coef))))

(def example (oa-map {1 2 3 4 5 6 7 8}))

(defn mymap-reduce
  ([f coll]
   (let [arr (get-not-empty coll)]
     (if > (count arr) 1)
     (mymap-reduce f (:val (first arr)) (rest arr))))
  ([f val coll]
   (loop [func f acc val rest-arr (get-not-empty coll)]
     (if (empty? rest-arr)
       acc
       (recur func (func acc (:val (first rest-arr))) (rest rest-arr))))))

(defn mymap-map [f coll]
  (let [full_arr (get-not-empty coll) new-arr (oa-map)]
    (loop [func f old-map full_arr new-map new-arr]
      (if (not (empty? old-map))
        (recur func (rest old-map) (assoc new-map (:key (first old-map)) (func (:val (first old-map)))))
        new-map))))

(ns lab2.core
  (:require [lab2.mymap :refer :all]
            [clojure.test.check :as tc]))

(defn -main []
  (let [test (oa-map {9 9 0 0})
        merged-hm (merge test example)
        dissoced-hm (dissoc merged-hm 1 2 9)
        updated-hm (assoc dissoced-hm 10 "NEW VALUE")
        get-value (get updated-hm 10)
        redused-map (mymap-reduce + example)
        mapped-map (mymap-map inc example)]
    (do
      (println "Example hash map: " example)
      (println "test hash map: " test)
      (println "Merged hash map: " merged-hm)
      (println "Dissoced hash map: " dissoced-hm)
      (println "Updated hash map: " updated-hm)
      (println "get y key 10: " get-value)
      (println "reduced example-map +: " redused-map)
      (println "map example-map inc: "  mapped-map))))
(ns lab2.core-test
  (:require [clojure.test :refer :all]
            [lab2.mymap :refer :all]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]))
(require '[clojure.test.check.clojure-test :refer [defspec]])

;;;;;;
(defn not-nil? [map]
  (if (:key (first map))
    true
    false))

(def my-map-entity-gen
  (gen/fmap (partial apply ->MyMapEntry) (gen/tuple gen/small-integer gen/small-integer)))

(def my-map-gen
  (gen/such-that not-nil? (gen/fmap (partial apply ->MyHashMap)
                                    (gen/tuple (gen/vector my-map-entity-gen) gen/small-integer))))

(defspec PBT-contains-key-prop
  100
  (prop/for-all [map-data my-map-gen]
                (let [curr-data map-data
                      ready-data (into [] curr-data)
                      rand-key (rand-int (count ready-data))]
                  (contains? map-data (:key (get ready-data rand-key))))))

(defspec PBT-dissoc-key-prop
  100
  (prop/for-all [map-data my-map-gen]
                (let [curr-data map-data
                      ready-data (into [] curr-data)
                      rand-key (rand-int (count ready-data))
                      dis-key (:key (get ready-data rand-key))
                      stripped (dissoc map-data dis-key)]
                  (not (contains? stripped dis-key)))))

(defspec PBT-mymap-reduce-prop
  100
  (prop/for-all [map-data my-map-gen]
                (let [all-data (into [] map-data)
                      all-val (map val all-data)
                      val-sum (reduce + all-val)]
                  (= (mymap-reduce + map-data) val-sum))))

(defspec PBT-mymap-map-prop
  100
  (prop/for-all [map-data my-map-gen]
                (let [all-data (into [] map-data)
                      rand-key (rand-int (count all-data))
                      mymap-map-result (mymap-map inc map-data)
                      default-map-result (map inc map-data)]
                  (= (get mymap-map-result rand-key) (get default-map-result rand-key)))))

(def full-map (oa-map (reduce #(assoc %1 %2 %2) {} (range 9))))
(def mixed-map (oa-map (reduce #(assoc %1 %2 %2) {} (range 5))))
(def empty-map (oa-map {}))

(deftest full-get-test
  (is (= (range 9) (map #(get full-map %) (range 9)))))

(deftest mixed-get-test
  (is (= (concat (range 5) (repeat 4 nil)) (map #(get mixed-map %) (range 9)))))

(deftest empty-get-test
  (is (= (repeat 9 nil) (map #(get empty-map %) (range 9)))))

(deftest full-insert-test
  (is (= (concat (range 13) [nil]) (map #(get (merge full-map {9 9 10 10 11 11 12 12}) %) (range 14)))))

(deftest mixed-insert-test
  (is (= (concat (range 5) (repeat 4 nil) (range 9 13) [nil]) (map #(get (merge mixed-map {9 9 10 10 11 11 12 12}) %) (range 14)))))

(deftest empty-insert-test
  (is (= (concat (repeat 9 nil) (range 9 13) [nil]) (map #(get (merge empty-map {9 9 10 10 11 11 12 12}) %) (range 14)))))

(deftest full-delete-test
  (is (= (concat (repeat 4 nil) (range 4 9) (repeat 5 nil)) (map #(get (dissoc full-map 0 1 2 3) %) (range 14)))))

(deftest mixed-delete-test
  (is (= (concat (repeat 4 nil) [4] (repeat 9 nil)) (map #(get (dissoc mixed-map 0 1 2 3) %) (range 14)))))

(deftest empty-delete-test
  (is (= (concat (repeat 14 nil)) (map #(get (dissoc empty-map 0 1 2 3) %) (range 14)))))

(deftest full-count-test
  (is (= 9 (count full-map))))

(deftest mixed-count-test
  (is (= 5 (count mixed-map))))

(deftest empty-count-test
  (is (= 0 (count empty-map))))

(deftest full-map-equiv-test
  (is (.equiv full-map (reduce #(assoc %1 %2 %2) {} (range 9)))))

(deftest mixed-map-equiv-test
  (is (.equiv mixed-map (reduce #(assoc %1 %2 %2) {} (range 5)))))

(deftest empty-map-equiv-test
  (is (.equiv empty-map {})))
# Лабораторная работа #2

**Дисциплина:** "Функциональное программирование"

**Выполнил:** Рождественский Никита, P34102

**Название:** "Реализация структуры данных"

**Цель работы:** 
 Освоиться с построением пользовательских типов данных, полиморфизмом, рекурсивными алгоритмами и средствами тестирования (unit testing, property-based testing).

**Требования**
1. Функции:
- добавление и удаление элементов;
- фильтрация;
- отображение (map);
- свертки (левая и правая);
- структура должна быть моноидом.
2. Структуры данных должны быть неизменяемыми.
3. Библиотека должна быть протестирована в рамках unit testing.
4. Библиотека должна быть протестирована в рамках property-based тестирования (как минимум 3 свойства, включая свойства монойда).
5. Структура должна быть полиморфной.
6. Требуется использовать идиоматичный для технологии стиль программирования.

## Вариант - Open addressing Hashmap
Описание типа элемента структуры 
```
(defrecord MyMapEntry [key val]
  IMapEntry
  (getKey [_] key)
  (getValue [_] val))
```
Описание типа структуры
```
(deftype MyHashMap [contents load]

  ILookup
  (valAt [m k]
    (if-let [[attempt _] (find-entry contents k)]
      (:val attempt)
      nil))

  Iterable
  (iterator [m] (.iterator (seq m)))

  Seqable
  (seq [_] (get-not-empty contents))

  IPersistentMap
  (assoc [_ k v] (apply ->MyHashMap (insert load contents (->MyMapEntry k v))))
  (assocEx [m k v] (if (.containsKey m k)
                     (.runtimeException Util "Key already present")
                     (.assoc m k v)))
  (without [_ k] (->MyHashMap (delete contents k) load))

  IPersistentCollection
  (count [m] (count (get-not-empty contents)))
  (cons [m new] (cond
                  (and (instance? IPersistentVector new) (>= (count new) 2)) (assoc m (first new) (nth new 2))
                  (instance? IMapEntry new) (assoc m (key new) (val new))
                  (instance? Seqable new) (reduce #(assoc %1 (key %2) (val %2)) m (seq new))))
  (empty [_] (->MyHashMap [] 1.0))
  (equiv [m o]
    (if (or
         (not (or (instance? Map o) (instance? IPersistentMap o)))
         (and
          (instance? IPersistentMap o)
          (->> o (instance? MapEquivalence) not))
         (not= (count o) (count m)))
      false
      (loop [elems (seq m)]
        (let [cur-elem (first elems)]
          (if-not (empty? elems)
            (if (or
                 (not (contains? o (.getKey cur-elem)))
                 (not (= (.getValue cur-elem) (get o (.getKey cur-elem)))))
              false
              (recur (rest elems)))
            true)))))
  Associative
  (containsKey [_ k] (let [[attempt _] (find-entry contents k)]
                       (if-not (nil? attempt)
                         true
                         false)))
  (entryAt [_ k] (first (find-entry contents k))))
```
Для поддержки большенства операций с хэш таблицей необходимо реализовать интерфейсы IPersistentMap, IPersistentCollection и Seqable.
## Property-based testing

```
(defn multy-run [test-fn times] (reduce #(and %1 %2) (repeatedly times test-fn)))

(defn generate-vec [size] (repeatedly size #(rand-int 10E+6)))

(defn contains-key-prop []
  (let [limit 1000
        data (generate-vec limit)
        generated (oa-map (reduce #(assoc %1 %2 %2) {} data))
        rnd-idx (rand-int limit)]
    (contains? generated (nth data rnd-idx))))

(deftest first-property
  (is (multy-run contains-key-prop 100)))

(defn dissoc-key-prop []
  (let [limit 1000
        data (generate-vec limit)
        generated (oa-map (reduce #(assoc %1 %2 %2) {} data))
        rnd-key (nth data (rand-int limit))
        stripped (dissoc generated rnd-key)]
    (not (contains? stripped rnd-key))))

(deftest second-property
  (is (multy-run dissoc-key-prop 100)))

(defn merge-key-prop []
  (let [limit 1000
        data-1 (generate-vec limit)
        data-2 (generate-vec limit)
        generated-1 (oa-map (reduce #(assoc %1 %2 %2) {} data-1))
        generated-2 (oa-map (reduce #(assoc %1 %2 %2) {} data-2))
        all-keys (reduce #(conj %1 %2) #{} (concat data-1 data-2))
        all-merged (merge generated-1 generated-2)]
    (reduce #(and %1 %2) (map #(contains? all-merged %) all-keys))))

(deftest third-property
  (is (multy-run merge-key-prop 100)))
```
## Пример использования
```
(defn -main []
  (let [test (oa-map {9 9 0 0})
        merged-hm (merge test example)
        dissoced-hm (dissoc merged-hm 1 2 9)
        updated-hm (assoc dissoced-hm 10 "NEW VALUE")
        get-value (get updated-hm 10)]
    (do
      (println "Example hash map: " example)
      (println "test hash map: " test)
      (println "Merged hash map: " merged-hm)
      (println "Dissoced hash map: " dissoced-hm)
      (println "Updated hash map: " updated-hm)
      (println "get y key 10: " get-value))))
```
Вывод:
```
kurtlike@debian:~/func/lab2$ clj -Mrun
Example hash map:  {1 2, 3 4, 7 8, 5 6}
test hash map:  {0 0, 9 9}
Merged hash map:  {0 0, 5 6, 1 2, 3 4, 7 8, 9 9}
Dissoced hash map:  {0 0, 5 6, 3 4, 7 8}
Updated hash map:  {0 0, 10 NEW VALUE, 3 4, 7 8, 5 6}
get y key 10:  NEW VALUE

```
## Заключение

В ходе выполнения данной работы пришлось много покапаться в исходниках языка и документации. Много было подсмотренно в https://github.com/clojure/clojure/blob/master/src/jvm/clojure/lang/PersistentHashMap.java
